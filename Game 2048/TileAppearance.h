//
//  TileAppearance.h
//  Game 2048
//
//  Created by Anshima on 07/02/17.
//  Copyright © 2017 Vikings Corp. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol TileAppearanceProtocol <NSObject>

- (UIColor *)tileColorForValue:(NSUInteger)value;
- (UIColor *)numberColorForValue:(NSUInteger)value;
- (UIFont *)fontForNumbers;

@end

@interface TileAppearance : NSObject <TileAppearanceProtocol>

@end

