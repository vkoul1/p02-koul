//
//  BoardView.m
//  Game 2048
//
//  Created by Vikas on 07/02/17.
//  Copyright © 2017 Vikings Corp. All rights reserved.
//

#import "BoardView.h"

#import <QuartzCore/QuartzCore.h>
#import "TileView.h"
#import "TileAppearance.h"

#define PER_SQUARE_SLIDE_DURATION 0.08

#if DEBUG
#define F3HLOG(...) NSLog(__VA_ARGS__)
#else
#define F3HLOG(...)
#endif

// Animation parameters
#define TILE_POP_START_SCALE    0.1
#define TILE_POP_MAX_SCALE      1.1
#define TILE_POP_DELAY          0.05
#define TILE_EXPAND_TIME        0.18
#define TILE_RETRACT_TIME       0.08

#define TILE_MERGE_START_SCALE  1.0
#define TILE_MERGE_EXPAND_TIME  0.08
#define TILE_MERGE_RETRACT_TIME 0.08


@interface BoardView ()

@property (nonatomic, strong) NSMutableDictionary *boardTiles;

@property (nonatomic) NSUInteger dimension;
@property (nonatomic) CGFloat tileSideLength;

@property (nonatomic) CGFloat padding;
@property (nonatomic) CGFloat cornerRadius;

@property (nonatomic, strong) TileAppearance *provider;

@end

@implementation BoardView

+ (instancetype)gameboardWithDimension:(NSUInteger)dimension
                             cellWidth:(CGFloat)width
                           cellPadding:(CGFloat)padding
                          cornerRadius:(CGFloat)cornerRadius
                       backgroundColor:(UIColor *)backgroundColor
                       foregroundColor:(UIColor *)foregroundColor {
    
    CGFloat sideLength = padding + dimension*(width + padding);
    BoardView *view = [[[self class] alloc] initWithFrame:CGRectMake(0,
                                                                            0,
                                                                            sideLength,
                                                                            sideLength)];
    view.dimension = dimension;
    view.padding = padding;
    view.tileSideLength = width;
    view.layer.cornerRadius = cornerRadius;
    view.cornerRadius = cornerRadius;
    [view setupBackgroundWithBackgroundColor:backgroundColor
                             foregroundColor:foregroundColor];
    return view;
}

- (void)setupBackgroundWithBackgroundColor:(UIColor *)background
                           foregroundColor:(UIColor *)foreground {
    self.backgroundColor = background;
    CGFloat xCursor = self.padding;
    CGFloat yCursor;
    CGFloat cornerRadius = self.cornerRadius - 2;
    if (cornerRadius < 0) {
        cornerRadius = 0;
    }
    for (NSInteger i=0; i<self.dimension; i++) {
        yCursor = self.padding;
        for (NSInteger j=0; j<self.dimension; j++) {
            UIView *bkgndTile = [[UIView alloc] initWithFrame:CGRectMake(xCursor,
                                                                         yCursor,
                                                                         self.tileSideLength,
                                                                         self.tileSideLength)];
            bkgndTile.layer.cornerRadius = cornerRadius;
            bkgndTile.backgroundColor = foreground;
            [self addSubview:bkgndTile];
            yCursor += self.padding + self.tileSideLength;
        }
        xCursor += self.padding + self.tileSideLength;
    }
}
- (TileAppearance *)provider {
    if (!_provider) {
        _provider = [TileAppearance new];
    }
    return _provider;
}

- (NSMutableDictionary *)boardTiles {
    if (!_boardTiles) {
        _boardTiles = [NSMutableDictionary dictionary];
    }
    return _boardTiles;
}

@end

