//
//  BoardModel.h
//  Game 2048
//
//  Created by Vikas on 07/02/17.
//  Copyright © 2017 Vikings Corp. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    MoveDirectionUp = 0,
    MoveDirectionDown,
    MoveDirectionLeft,
    MoveDirectionRight
} MoveDirection;


@interface GameModel : NSObject

@property (nonatomic, readonly) NSInteger score;

+ (instancetype)gameModelWithDimension:(NSUInteger)dimension
                              winValue:(NSUInteger)value;

@end
