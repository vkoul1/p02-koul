//
//  BoardView.h
//  Game 2048
//
//  Created by Vikas on 07/02/17.
//  Copyright © 2017 Vikings Corp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BoardView : UIView

+ (instancetype)gameboardWithDimension:(NSUInteger)dimension
                             cellWidth:(CGFloat)width
                           cellPadding:(CGFloat)padding
                          cornerRadius:(CGFloat)cornerRadius
                       backgroundColor:(UIColor *)backgroundColor
                       foregroundColor:(UIColor *)foregroundColor;

@end
