//
//  QueueCommand.m
//  Game 2048
//
//  Created by Vikas on 08/02/17.
//  Copyright © 2017 Vikings Corp. All rights reserved.
//

#import "QueueCommand.h"

@implementation QueueCommand

+ (instancetype)commandWithDirection:(MoveDirection)direction
                     completionBlock:(void(^)(BOOL))completion {
    QueueCommand *command = [[self class] new];
    command.direction = direction;
    command.completion = completion;
    return command;
}

@end
