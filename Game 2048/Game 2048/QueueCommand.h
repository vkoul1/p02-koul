//
//  QueueCommand.h
//  Game 2048
//
//  Created by Vikas on 08/02/17.
//  Copyright © 2017 Vikings Corp. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GameModel.h"

@interface QueueCommand : NSObject

@property (nonatomic) MoveDirection direction;
@property (nonatomic, copy) void(^completion)(BOOL atLeastOneMove);

+ (instancetype)commandWithDirection:(MoveDirection)direction completionBlock:(void(^)(BOOL))completion;

@end
