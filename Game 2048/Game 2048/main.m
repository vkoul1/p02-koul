//
//  main.m
//  Game 2048
//
//  Created by Vikas on 06/02/17.
//  Copyright © 2017 Vikings Corp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
