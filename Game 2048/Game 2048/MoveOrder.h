//
//  MoveOrder.h
//  Game 2048
//
//  Created by Vikas on 08/02/17.
//  Copyright © 2017 Vikings Corp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MoveOrder : NSObject

@property (nonatomic) NSInteger source1;
@property (nonatomic) NSInteger source2;
@property (nonatomic) NSInteger destination;
@property (nonatomic) BOOL doubleMove;
@property (nonatomic) NSInteger value;

+ (instancetype)singleMoveOrderWithSource:(NSInteger)source
                              destination:(NSInteger)destination
                                 newValue:(NSInteger)value;

+ (instancetype)doubleMoveOrderWithFirstSource:(NSInteger)source1
                                  secondSource:(NSInteger)source2
                                   destination:(NSInteger)destination
                                      newValue:(NSInteger)value;

@end

