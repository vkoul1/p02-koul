//
//  ControlView.m
//  Game 2048
//
//  Created by Vikas on 08/02/17.
//  Copyright © 2017 Vikings Corp. All rights reserved.
//

#import "ControlView.h"

#define FRAME_SIZE    CGRectMake(0, 0, 230, 140)

@interface  ControlView ()
@property (nonatomic) BOOL moveButtonsEnabled;
@property (nonatomic) BOOL exitButtonEnabled;
@property (nonatomic, weak) id< ControlViewProtocol> delegate;
@end

@implementation  ControlView

+ (instancetype)controlViewWithCornerRadius:(CGFloat)radius
                            backgroundColor:(UIColor *)color
                            movementButtons:(BOOL)moveButtonsEnabled
                                 exitButton:(BOOL)exitButtonEnabled
                                   delegate:(id< ControlViewProtocol>)delegate {
     ControlView *view = [[[self class] alloc] initWithFrame: FRAME_SIZE];
    view.moveButtonsEnabled = moveButtonsEnabled;
    view.exitButtonEnabled = exitButtonEnabled;
    view.backgroundColor = color ?: [UIColor darkGrayColor];
    view.layer.cornerRadius = radius;
    view.delegate = delegate;
    [view setupSubviews];
    return view;
}

- (void)setupSubviews {
    if (self.moveButtonsEnabled) {
        // Large layout
        UIButton *upButton = [[UIButton alloc] initWithFrame:CGRectMake(90, 10, 50, 50)];
        upButton.layer.cornerRadius = 10;
        upButton.backgroundColor = [UIColor grayColor];
        upButton.titleLabel.textColor = [UIColor whiteColor];
        upButton.showsTouchWhenHighlighted = YES;
        [upButton addTarget:self
                     action:@selector(upButtonTapped)
           forControlEvents:UIControlEventTouchUpInside];
        [upButton setTitle:@"Up" forState:UIControlStateNormal];
        [self addSubview:upButton];
        
        UIButton *downButton = [[UIButton alloc] initWithFrame:CGRectMake(90, 80, 50, 50)];
        downButton.layer.cornerRadius = 10;
        downButton.backgroundColor = [UIColor grayColor];
        downButton.titleLabel.textColor = [UIColor whiteColor];
        downButton.showsTouchWhenHighlighted = YES;
        [downButton addTarget:self
                       action:@selector(downButtonTapped)
             forControlEvents:UIControlEventTouchUpInside];
        [downButton setTitle:@"Down" forState:UIControlStateNormal];
        [self addSubview:downButton];
        
        UIButton *leftButton = [[UIButton alloc] initWithFrame:CGRectMake(20, 62, 60, 50)];
        leftButton.layer.cornerRadius = 10;
        leftButton.backgroundColor = [UIColor grayColor];
        leftButton.titleLabel.textColor = [UIColor whiteColor];
        leftButton.showsTouchWhenHighlighted = YES;
        [leftButton addTarget:self
                       action:@selector(leftButtonTapped)
             forControlEvents:UIControlEventTouchUpInside];
        [leftButton setTitle:@"Left" forState:UIControlStateNormal];
        [self addSubview:leftButton];
        
        UIButton *rightButton = [[UIButton alloc] initWithFrame:CGRectMake(150, 62, 60, 50)];
        rightButton.layer.cornerRadius = 10;
        rightButton.backgroundColor = [UIColor grayColor];
        rightButton.titleLabel.textColor = [UIColor whiteColor];
        rightButton.showsTouchWhenHighlighted = YES;
        [rightButton addTarget:self
                        action:@selector(rightButtonTapped)
              forControlEvents:UIControlEventTouchUpInside];
        [rightButton setTitle:@"Right" forState:UIControlStateNormal];
        [self addSubview:rightButton];
        
        UIButton *resetButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 70, 30)];
        resetButton.titleLabel.textColor = [UIColor whiteColor];
        resetButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14];
        resetButton.showsTouchWhenHighlighted = YES;
        [resetButton addTarget:self
                        action:@selector(resetButtonTapped)
              forControlEvents:UIControlEventTouchUpInside];
        [resetButton setTitle:@"RESET" forState:UIControlStateNormal];
        [self addSubview:resetButton];
        
        if (self.exitButtonEnabled) {
            UIButton *exitButton = [[UIButton alloc] initWithFrame:CGRectMake(160, 0, 70, 30)];
            exitButton.titleLabel.textColor = [UIColor whiteColor];
            exitButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14];
            exitButton.showsTouchWhenHighlighted = YES;
            [exitButton setTitle:@"EXIT" forState:UIControlStateNormal];
            [exitButton addTarget:self
                           action:@selector(exitButtonTapped)
                 forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:exitButton];
        }
    }
    else {
        // Small layout
        UIButton *resetButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 70, 30)];
        resetButton.titleLabel.textColor = [UIColor whiteColor];
        resetButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14];
        resetButton.showsTouchWhenHighlighted = YES;
        [resetButton setTitle:@"RESET" forState:UIControlStateNormal];
        [resetButton addTarget:self
                        action:@selector(resetButtonTapped)
              forControlEvents:UIControlEventTouchUpInside];
        [resetButton setTitle:@"RESET" forState:UIControlStateNormal];
        [self addSubview:resetButton];
        
        if (self.exitButtonEnabled) {
            UIButton *exitButton = [[UIButton alloc] initWithFrame:CGRectMake(160, 0, 70, 30)];
            exitButton.titleLabel.textColor = [UIColor whiteColor];
            exitButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14];
            exitButton.showsTouchWhenHighlighted = YES;
            [exitButton setTitle:@"EXIT" forState:UIControlStateNormal];
            [exitButton addTarget:self
                           action:@selector(exitButtonTapped)
                 forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:exitButton];
        }
    }
}

- (void)upButtonTapped {
    [self.delegate upButtonTapped];
}

- (void)downButtonTapped {
    [self.delegate downButtonTapped];
}

- (void)leftButtonTapped {
    [self.delegate leftButtonTapped];
}

- (void)rightButtonTapped {
    [self.delegate rightButtonTapped];
}

- (void)resetButtonTapped {
    [self.delegate resetButtonTapped];
}

- (void)exitButtonTapped {
    [self.delegate exitButtonTapped];
}

@end

