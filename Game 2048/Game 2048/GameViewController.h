//
//  GameViewController.h
//  Game 2048
//
//  Created by Vikas on 08/02/17.
//  Copyright © 2017 Vikings Corp. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol GameProtocol <NSObject>
- (void)gameFinishedWithVictory:(BOOL)didWin score:(NSInteger)score;
@end

@interface GameViewController : UIViewController

@property (nonatomic, weak) id<GameProtocol>delegate;

+ (instancetype)numberTileGameWithDimension:(NSUInteger)dimension
                               winThreshold:(NSUInteger)threshold
                            backgroundColor:(UIColor *)backgroundColor
                                scoreModule:(BOOL)scoreModuleEnabled
                             buttonControls:(BOOL)buttonControlsEnabled
                              swipeControls:(BOOL)swipeControlsEnabled;

@end
