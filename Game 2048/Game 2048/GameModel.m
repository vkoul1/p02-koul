//
//  GameModel.m
//  Game 2048
//
//  Created by Vikas on 08/02/17.
//  Copyright © 2017 Vikings Corp. All rights reserved.
//

#import "GameModel.h"
#import "MoveOrder.h"
#import "TileModel.h"
#import "MergeTile.h"
#import "QueueCommand.h"

#define MAX_COMMANDS      100
#define QUEUE_DELAY       0.3

@interface GameModel ()

@property (nonatomic, weak) id<GameModelProtocol> delegate;


@property (nonatomic) NSUInteger dimension;
@property (nonatomic) NSUInteger winValue;

@property (nonatomic, strong) NSMutableArray *gameState;

@property (nonatomic, strong) NSMutableArray *commandQueue;
@property (nonatomic, strong) NSTimer *queueTimer;

@property (nonatomic, readwrite) NSInteger score;

@end

@implementation GameModel

+ (instancetype)gameModelWithDimension:(NSUInteger)dimension
                              winValue:(NSUInteger)value
                              delegate:(id<GameModelProtocol>)delegate {
    GameModel *model = [GameModel new];
    model.dimension = dimension;
    model.winValue = value;
    model.delegate = delegate;
    [model reset];
    return model;
}

- (void)reset {
    self.score = 0;
    self.gameState = nil;
    [self.commandQueue removeAllObjects];
    [self.queueTimer invalidate];
    self.queueTimer = nil;
}

#pragma mark - Insertion API

- (void)insertAtRandomLocationTileWithValue:(NSUInteger)value {
    BOOL emptySpotFound = NO;
    for (NSInteger i=0; i<[self.gameState count]; i++) {
        if (((TileModel *) self.gameState[i]).empty) {
            emptySpotFound = YES;
            break;
        }
    }
    if (!emptySpotFound) {
        return;
    }
    NSInteger row = 0;
    BOOL shouldExit = NO;
    while (YES) {
        row = arc4random_uniform((uint32_t)self.dimension);
        for (NSInteger i=0; i<self.dimension; i++) {
            if ([self tileForIndexPath:[NSIndexPath indexPathForRow:row inSection:i]].empty) {
                shouldExit = YES;
                break;
            }
        }
        if (shouldExit) {
            break;
        }
    }
    NSInteger column = 0;
    shouldExit = NO;
    while (YES) {
        column = arc4random_uniform((uint32_t)self.dimension);
        if ([self tileForIndexPath:[NSIndexPath indexPathForRow:row inSection:column]].empty) {
            shouldExit = YES;
            break;
        }
        if (shouldExit) {
            break;
        }
    }
    [self insertTileWithValue:value atIndexPath:[NSIndexPath indexPathForRow:row inSection:column]];
}

- (void)insertTileWithValue:(NSUInteger)value
                atIndexPath:(NSIndexPath *)path {
    if (![self tileForIndexPath:path].empty) {
        return;
    }
    TileModel *tile = [self tileForIndexPath:path];
    tile.empty = NO;
    tile.value = value;
    [self.delegate insertTileAtIndexPath:path value:value];
}

- (void)performMoveInDirection:(MoveDirection)direction
               completionBlock:(void(^)(BOOL))completion {
    [self queueCommand:[QueueCommand commandWithDirection:direction completionBlock:completion]];
}

- (BOOL)performUpMove {
    BOOL atLeastOneMove = NO;
    
    for (NSInteger column = 0; column<self.dimension; column++) {
        NSMutableArray *thisColumnTiles = [NSMutableArray arrayWithCapacity:self.dimension];
        for (NSInteger row = 0; row<self.dimension; row++) {
            [thisColumnTiles addObject:[self tileForIndexPath:[NSIndexPath indexPathForRow:row inSection:column]]];
        }
        NSArray *ordersArray = [self mergeGroup:thisColumnTiles];
        if ([ordersArray count] > 0) {
            atLeastOneMove = YES;
            for (NSInteger i=0; i<[ordersArray count]; i++) {
                MoveOrder *order = ordersArray[i];
                if (order.doubleMove) {
                    
                    NSIndexPath *source1Path = [NSIndexPath indexPathForRow:order.source1 inSection:column];
                    NSIndexPath *source2Path = [NSIndexPath indexPathForRow:order.source2 inSection:column];
                    NSIndexPath *destinationPath = [NSIndexPath indexPathForRow:order.destination inSection:column];
                    
                    TileModel *source1Tile = [self tileForIndexPath:source1Path];
                    source1Tile.empty = YES;
                    TileModel *source2Tile = [self tileForIndexPath:source2Path];
                    source2Tile.empty = YES;
                    TileModel *destinationTile = [self tileForIndexPath:destinationPath];
                    destinationTile.empty = NO;
                    destinationTile.value = order.value;
                    
                    [self.delegate moveTileOne:source1Path
                                       tileTwo:source2Path
                                   toIndexPath:destinationPath
                                      newValue:order.value];
                }
                else {
                    NSIndexPath *sourcePath = [NSIndexPath indexPathForRow:order.source1 inSection:column];
                    NSIndexPath *destinationPath = [NSIndexPath indexPathForRow:order.destination inSection:column];
                    
                    TileModel *sourceTile = [self tileForIndexPath:sourcePath];
                    sourceTile.empty = YES;
                    TileModel *destinationTile = [self tileForIndexPath:destinationPath];
                    destinationTile.empty = NO;
                    destinationTile.value = order.value;
                    
                    [self.delegate moveTileFromIndexPath:sourcePath
                                             toIndexPath:destinationPath
                                                newValue:order.value];
                }
            }
        }
    }
    return atLeastOneMove;
}

- (BOOL)performDownMove {
    BOOL atLeastOneMove = NO;
    
    for (NSInteger column = 0; column<self.dimension; column++) {
        NSMutableArray *thisColumnTiles = [NSMutableArray arrayWithCapacity:self.dimension];
        for (NSInteger row = (self.dimension - 1); row >= 0; row--) {
            [thisColumnTiles addObject:[self tileForIndexPath:[NSIndexPath indexPathForRow:row inSection:column]]];
        }
        NSArray *ordersArray = [self mergeGroup:thisColumnTiles];
        if ([ordersArray count] > 0) {
            atLeastOneMove = YES;
            for (NSInteger i=0; i<[ordersArray count]; i++) {
                MoveOrder *order = ordersArray[i];
                NSInteger dim = self.dimension - 1;
                if (order.doubleMove) {
                    
                    NSIndexPath *source1Path = [NSIndexPath indexPathForRow:(dim - order.source1) inSection:column];
                    NSIndexPath *source2Path = [NSIndexPath indexPathForRow:(dim - order.source2) inSection:column];
                    NSIndexPath *destinationPath = [NSIndexPath indexPathForRow:(dim - order.destination) inSection:column];
                    
                    TileModel *source1Tile = [self tileForIndexPath:source1Path];
                    source1Tile.empty = YES;
                    TileModel *source2Tile = [self tileForIndexPath:source2Path];
                    source2Tile.empty = YES;
                    TileModel *destinationTile = [self tileForIndexPath:destinationPath];
                    destinationTile.empty = NO;
                    destinationTile.value = order.value;
                    
                    
                    [self.delegate moveTileOne:source1Path
                                       tileTwo:source2Path
                                   toIndexPath:destinationPath
                                      newValue:order.value];
                }
                else {
                    
                    NSIndexPath *sourcePath = [NSIndexPath indexPathForRow:(dim - order.source1) inSection:column];
                    NSIndexPath *destinationPath = [NSIndexPath indexPathForRow:(dim - order.destination) inSection:column];
                    
                    TileModel *sourceTile = [self tileForIndexPath:sourcePath];
                    sourceTile.empty = YES;
                    TileModel *destinationTile = [self tileForIndexPath:destinationPath];
                    destinationTile.empty = NO;
                    destinationTile.value = order.value;
                    
                    
                    [self.delegate moveTileFromIndexPath:sourcePath
                                             toIndexPath:destinationPath
                                                newValue:order.value];
                }
            }
        }
    }
    return atLeastOneMove;
}

- (BOOL)performLeftMove {
    BOOL atLeastOneMove = NO;
    
    for (NSInteger row = 0; row<self.dimension; row++) {
        NSMutableArray *thisRowTiles = [NSMutableArray arrayWithCapacity:self.dimension];
        for (NSInteger column = 0; column<self.dimension; column++) {
            [thisRowTiles addObject:[self tileForIndexPath:[NSIndexPath indexPathForRow:row inSection:column]]];
        }
        NSArray *ordersArray = [self mergeGroup:thisRowTiles];
        if ([ordersArray count] > 0) {
            atLeastOneMove = YES;
            for (NSInteger i=0; i<[ordersArray count]; i++) {
                MoveOrder *order = ordersArray[i];
                if (order.doubleMove) {
                    NSIndexPath *source1Path = [NSIndexPath indexPathForRow:row inSection:order.source1];
                    NSIndexPath *source2Path = [NSIndexPath indexPathForRow:row inSection:order.source2];
                    NSIndexPath *destinationPath = [NSIndexPath indexPathForRow:row inSection:order.destination];
                    
                    TileModel *source1Tile = [self tileForIndexPath:source1Path];
                    source1Tile.empty = YES;
                    TileModel *source2Tile = [self tileForIndexPath:source2Path];
                    source2Tile.empty = YES;
                    TileModel *destinationTile = [self tileForIndexPath:destinationPath];
                    destinationTile.empty = NO;
                    destinationTile.value = order.value;
                    
                    [self.delegate moveTileOne:source1Path
                                       tileTwo:source2Path
                                   toIndexPath:destinationPath
                                      newValue:order.value];
                }
                else {
                    
                    NSIndexPath *sourcePath = [NSIndexPath indexPathForRow:row inSection:order.source1];
                    NSIndexPath *destinationPath = [NSIndexPath indexPathForRow:row inSection:order.destination];
                    
                    TileModel *sourceTile = [self tileForIndexPath:sourcePath];
                    sourceTile.empty = YES;
                    TileModel *destinationTile = [self tileForIndexPath:destinationPath];
                    destinationTile.empty = NO;
                    destinationTile.value = order.value;
                    
                    [self.delegate moveTileFromIndexPath:sourcePath
                                             toIndexPath:destinationPath
                                                newValue:order.value];
                }
            }
        }
    }
    return atLeastOneMove;
}

- (BOOL)performRightMove {
    BOOL atLeastOneMove = NO;
    
    for (NSInteger row = 0; row<self.dimension; row++) {
        NSMutableArray *thisRowTiles = [NSMutableArray arrayWithCapacity:self.dimension];
        for (NSInteger column = (self.dimension - 1); column >= 0; column--) {
            [thisRowTiles addObject:[self tileForIndexPath:[NSIndexPath indexPathForRow:row inSection:column]]];
        }
        NSArray *ordersArray = [self mergeGroup:thisRowTiles];
        if ([ordersArray count] > 0) {
            NSInteger dim = self.dimension - 1;
            atLeastOneMove = YES;
            for (NSInteger i=0; i<[ordersArray count]; i++) {
                MoveOrder *order = ordersArray[i];
                if (order.doubleMove) {
                    
                    NSIndexPath *source1Path = [NSIndexPath indexPathForRow:row inSection:(dim - order.source1)];
                    NSIndexPath *source2Path = [NSIndexPath indexPathForRow:row inSection:(dim - order.source2)];
                    NSIndexPath *destinationPath = [NSIndexPath indexPathForRow:row inSection:(dim - order.destination)];
                    
                    TileModel *source1Tile = [self tileForIndexPath:source1Path];
                    source1Tile.empty = YES;
                    TileModel *source2Tile = [self tileForIndexPath:source2Path];
                    source2Tile.empty = YES;
                    TileModel *destinationTile = [self tileForIndexPath:destinationPath];
                    destinationTile.empty = NO;
                    destinationTile.value = order.value;
                    
                    [self.delegate moveTileOne:source1Path
                                       tileTwo:source2Path
                                   toIndexPath:destinationPath
                                      newValue:order.value];
                }
                else {
                    NSIndexPath *sourcePath = [NSIndexPath indexPathForRow:row inSection:(dim - order.source1)];
                    NSIndexPath *destinationPath = [NSIndexPath indexPathForRow:row inSection:(dim - order.destination)];
                    
                    TileModel *sourceTile = [self tileForIndexPath:sourcePath];
                    sourceTile.empty = YES;
                    TileModel *destinationTile = [self tileForIndexPath:destinationPath];
                    destinationTile.empty = NO;
                    destinationTile.value = order.value;
                    
                    [self.delegate moveTileFromIndexPath:sourcePath
                                             toIndexPath:destinationPath
                                                newValue:order.value];
                }
            }
        }
    }
    return atLeastOneMove;
}


#pragma mark - Game State API

- (BOOL)userHasLost {
    for (NSInteger i=0; i<[self.gameState count]; i++) {
        if (((TileModel *) self.gameState[i]).empty) {
            return NO;
        }
    }
    for (NSInteger i=0; i<self.dimension; i++) {
        for (NSInteger j=0; j<self.dimension; j++) {
            TileModel *tile = [self tileForIndexPath:[NSIndexPath indexPathForRow:i inSection:j]];
            if (j != (self.dimension - 1)
                && tile.value == [self tileForIndexPath:[NSIndexPath indexPathForRow:i inSection:j+1]].value) {
                return NO;
            }
            if (i != (self.dimension - 1)
                && tile.value == [self tileForIndexPath:[NSIndexPath indexPathForRow:i+1 inSection:j]].value) {
                return NO;
            }
        }
    }
    return YES;
}

- (NSIndexPath *)userHasWon {
    for (NSInteger i=0; i<[self.gameState count]; i++) {
        if (((TileModel *) self.gameState[i]).value == self.winValue) {
            return [NSIndexPath indexPathForRow:(i / self.dimension)
                                      inSection:(i % self.dimension)];
        }
    }
    return nil;
}


#pragma mark - Private Methods

- (void)queueCommand:(QueueCommand *)command {
    if (!command || [self.commandQueue count] > MAX_COMMANDS) return;
    
    [self.commandQueue addObject:command];
    if (!self.queueTimer || ![self.queueTimer isValid]) {
        [self timerFired:nil];
    }
}

- (void)timerFired:(NSTimer *)timer {
    if ([self.commandQueue count] == 0) return;
    
    BOOL changed = NO;
    while ([self.commandQueue count] > 0) {
        QueueCommand *command = [self.commandQueue firstObject];
        [self.commandQueue removeObjectAtIndex:0];
        switch (command.direction) {
            case MoveDirectionUp:
                changed = [self performUpMove];
                break;
            case MoveDirectionDown:
                changed = [self performDownMove];
                break;
            case MoveDirectionLeft:
                changed = [self performLeftMove];
                break;
            case MoveDirectionRight:
                changed = [self performRightMove];
                break;
        }
        if (command.completion) {
            command.completion(changed);
        }
        if (changed) {
            break;
        }
    }
    
    self.queueTimer = [NSTimer scheduledTimerWithTimeInterval:QUEUE_DELAY
                                                       target:self
                                                     selector:@selector(timerFired:)
                                                     userInfo:nil
                                                      repeats:NO];
}

- (TileModel *)tileForIndexPath:(NSIndexPath *)indexPath {
    NSInteger idx = (indexPath.row*self.dimension + indexPath.section);
    if (idx >= [self.gameState count]) {
        return nil;
    }
    return self.gameState[idx];
}

- (void)setTile:(TileModel *)tile forIndexPath:(NSIndexPath *)indexPath {
    NSInteger idx = (indexPath.row*self.dimension + indexPath.section);
    if (!tile || idx >= [self.gameState count]) {
        return;
    }
    self.gameState[idx] = tile;
}

- (NSMutableArray *)commandQueue {
    if (!_commandQueue) {
        _commandQueue = [NSMutableArray array];
    }
    return _commandQueue;
}

- (NSMutableArray *)gameState {
    if (!_gameState) {
        _gameState = [NSMutableArray array];
        for (NSInteger i=0; i<(self.dimension * self.dimension); i++) {
            [_gameState addObject:[TileModel emptyTile]];
        }
    }
    return _gameState;
}

- (void)setScore:(NSInteger)score {
    _score = score;
    [self.delegate scoreChanged:score];
}

- (NSArray *)mergeGroup:(NSArray *)group {
    NSInteger ctr = 0;
    NSMutableArray *stack1 = [NSMutableArray array];
    for (NSInteger i=0; i<self.dimension; i++) {
        TileModel *tile = group[i];
        if (tile.empty) {
            continue;
        }
        MergeTile *mergeTile = [MergeTile mergeTile];
        mergeTile.originalIndexA = i;
        mergeTile.value = tile.value;
        if (i == ctr) {
            mergeTile.mode = MergeTileModeNoAction;
        }
        else {
            mergeTile.mode = MergeTileModeMove;
        }
        [stack1 addObject:mergeTile];
        ctr++;
    }
    if ([stack1 count] == 0) {
        return nil;
    }
    else if ([stack1 count] == 1) {
        if (((MergeTile *)stack1[0]).mode == MergeTileModeMove) {
            MergeTile *mTile = (MergeTile *)stack1[0];
            return @[[MoveOrder singleMoveOrderWithSource:mTile.originalIndexA
                                              destination:0
                                                 newValue:mTile.value]];
        }
        else {
            return nil;
        }
    }
    ctr = 0;
    BOOL priorMergeHasHappened = NO;
    NSMutableArray *stack2 = [NSMutableArray array];
    while (ctr < ([stack1 count] - 1)) {
        MergeTile *t1 = (MergeTile *)stack1[ctr];
        MergeTile *t2 = (MergeTile *)stack1[ctr+1];
        if (t1.value == t2.value) {
            NSAssert(t1.mode != MergeTileModeSingleCombine && t2.mode != MergeTileModeSingleCombine
                     && t1.mode != MergeTileModeDoubleCombine && t2.mode != MergeTileModeDoubleCombine,
                     @"Should not be able to get in a state where already-combined tiles are recombined");
            
            if (t1.mode == MergeTileModeNoAction && !priorMergeHasHappened) {
                priorMergeHasHappened = YES;
                MergeTile *newT = [MergeTile mergeTile];
                newT.mode = MergeTileModeSingleCombine;
                newT.originalIndexA = t2.originalIndexA;
                newT.value = t1.value * 2;
                self.score += newT.value;
                [stack2 addObject:newT];
            }
            else {
                MergeTile *newT = [MergeTile mergeTile];
                newT.mode = MergeTileModeDoubleCombine;
                newT.originalIndexA = t1.originalIndexA;
                newT.originalIndexB = t2.originalIndexA;
                newT.value = t1.value * 2;
                self.score += newT.value;
                [stack2 addObject:newT];
            }
            ctr += 2;
        }
        else {
            [stack2 addObject:t1];
            if ([stack2 count] - 1 != ctr) {
                t1.mode = MergeTileModeMove;
            }
            ctr++;
        }
        if (ctr == [stack1 count] - 1) {
            MergeTile *item = stack1[ctr];
            [stack2 addObject:item];
            if ([stack2 count] - 1 != ctr) {
                item.mode = MergeTileModeMove;
            }
        }
    }
    
    NSMutableArray *stack3 = [NSMutableArray new];
    for (NSInteger i=0; i<[stack2 count]; i++) {
        MergeTile *mTile = stack2[i];
        switch (mTile.mode) {
            case MergeTileModeEmpty:
            case MergeTileModeNoAction:
                continue;
            case MergeTileModeMove:
            case MergeTileModeSingleCombine:
                [stack3 addObject:[MoveOrder singleMoveOrderWithSource:mTile.originalIndexA
                                                           destination:i
                                                              newValue:mTile.value]];
                break;
            case MergeTileModeDoubleCombine:
                [stack3 addObject:[MoveOrder doubleMoveOrderWithFirstSource:mTile.originalIndexA
                                                               secondSource:mTile.originalIndexB
                                                                destination:i
                                                                   newValue:mTile.value]];
                break;
        }
    }
    return [NSArray arrayWithArray:stack3];
}

@end
