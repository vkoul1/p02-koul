//
//  MergeTile.h
//  Game 2048
//
//  Created by Vikas on 08/02/17.
//  Copyright © 2017 Vikings Corp. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    MergeTileModeEmpty = 0,
    MergeTileModeNoAction,
    MergeTileModeMove,
    MergeTileModeSingleCombine,
    MergeTileModeDoubleCombine
} MergeTileMode;

@interface MergeTile : NSObject

@property (nonatomic) MergeTileMode mode;
@property (nonatomic) NSInteger originalIndexA;
@property (nonatomic) NSInteger originalIndexB;
@property (nonatomic) NSInteger value;

+ (instancetype)mergeTile;

@end
