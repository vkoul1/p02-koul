//
//  ViewController.m
//  Game 2048
//
//  Created by Vikas on 06/02/17.
//  Copyright © 2017 Vikings Corp. All rights reserved.
//

#import "ViewController.h"
#import "GameViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (IBAction)playGame:(id)sender{
    GameViewController *game = [GameViewController numberTileGameWithDimension:4
                                                                  winThreshold:2048
                                                               backgroundColor:[UIColor whiteColor]
                                                                   scoreModule:YES
                                                                buttonControls:YES
                                                                 swipeControls:NO];
    [self presentViewController:game animated:YES completion:nil];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

