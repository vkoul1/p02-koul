//
//  TileView.h
//  Game 2048
//
//  Created by Vikas on 07/02/17.
//  Copyright © 2017 Vikings Corp. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TileAppearanceProtocol;
@interface TileView : UIView

@property (nonatomic) NSInteger tileValue;

@property (nonatomic, weak) id<TileAppearanceProtocol>delegate;

+ (instancetype)tileForPosition:(CGPoint)position
                     sideLength:(CGFloat)side
                          value:(NSUInteger)value
                   cornerRadius:(CGFloat)cornerRadius;

@end
